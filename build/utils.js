const path = require('path');

module.exports = {
  assetsPath (_path) {

    const assetsSubDirectory = 'static';

    return path.posix.join(assetsSubDirectory, _path);

  },
  resolve (dir) {

    return path.join(__dirname, '..', dir);

  },

}