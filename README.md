# Vuejs Webpack4 Template

Webpack Template for Vue.js Application- Webpack 4, SCSS, Eslint 5, Babel 7, vue-router, vuex and other helpful additions.

## Running dev server

`yarn run dev`


## Building for production

`yarn run prod`