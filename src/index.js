/* eslint-disable no-unused-vars */
import '../assets/app.scss';

import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import {library} from '@fortawesome/fontawesome-svg-core'
import {faUserSecret} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

// Add icon here
library.add(faUserSecret);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Vuex);

new Vue({
  render: h => h(App),
}).$mount('#app');